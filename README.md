vagrant-javadev-box
================

Build a basic Vagrant box set for Java development with Maven and thift installed.

## Installation

* If you haven't already, go over to [http://www.vagrantup.com/](http://www.vagrantup.com/) and follow the installation instructions
* Add the box `precise64` via `vagrant box add precise64 http://files.vagrantup.com/precise64.box`
* Clone this repo `git clone git@bitbucket.org:jstapleton/vagrant-javadev-box-master.git`
* BUG: For some reason the setup.sh didn't auto provision so *before* running `vagrant up` run `vagrant provision`
* All done; `vagrant up`
* BUG: the java and mvn binaries are not in the path.  

## Customization
Adding your own sync'd folders.  This is easiest done by editing: ~/.vagrant.d/Vagrantfile

```
Vagrant.configure("2") do |config|
  config.vm.synced_folder "~/workspaces/orion", "/home/vagrant/orion"
  config.vm.synced_folder "~/.m2", "/home/vagrant/.m2"
end
```

## Usage

Feel free to customise further or use as is.

The directory `app` is sync'd to `/home/vagrant/app` so place your code in there and hack away.

Jump onto the VM by `vagrant ssh`.