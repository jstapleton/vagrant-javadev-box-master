#!/bin/bash

sudo apt-get update --fix-missing

sudo apt-get install -y curl git vim

#install java
tar -zxf /vagrant/provision/jdk-7u45-linux-x64.tar.gz
echo "export JAVA_HOME=$HOME/jdk1.7.0_45"  >> $HOME/.bashrc
echo "export PATH=$PATH:$JAVA_HOME/bin" >> $HOME/.bashrc
export JAVA_HOME=$HOME/jdk1.7.0_45
export PATH=$PATH:$JAVA_HOME/bin

#install apache maven
curl -O http://apache.mirrors.lucidnetworks.net/maven/maven-3/3.1.1/binaries/apache-maven-3.1.1-bin.tar.gz
tar -zxf apache-maven-3.1.1-bin.tar.gz
echo "export PATH=$PATH:$HOME/apache-maven-3.1.1/bin" >> $HOME/.bashrc
export PATH=$PATH:$HOME/apache-maven-3.1.1/bin

#install apache ant
curl -O http://mirror.cogentco.com/pub/apache//ant/binaries/apache-ant-1.9.2-bin.tar.gz
tar -zxf apache-ant-1.9.2-bin.tar.gz
echo "export ANT_HOME=$HOME/apache-ant-1.9.2"  >> $HOME/.bashrc
echo "export PATH=$PATH:$ANT_HOME/bin" >> $HOME/.bashrc
export ANT_HOME=$HOME/apache-ant-1.9.2
export PATH=$PATH:$ANT_HOME/bin

# install dependencies of thrift
sudo apt-get install -y make libboost-dev libboost-test-dev libboost-program-options-dev libevent-dev automake libtool flex bison pkg-config g++ libssl-dev 

# precompiled binary version
tar -zxf /vagrant/provision/thrift-0.6.1-bin.tar.gz
cd thrift-0.6.1
sudo make install
